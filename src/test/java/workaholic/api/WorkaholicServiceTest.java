package workaholic.api;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class WorkaholicServiceTest {

    private WorkaholicService workaholicService = new WorkaholicService();

    @Test
    public void testIsItWeekend() {
        String dateStr = "2019-11-25 05:59";
        Assert.isTrue(workaholicService.isItWeekend(dateStr), "Incorrect weekend detection for given date.");

        dateStr = "2019-11-25 06:00";
        Assert.isTrue(!workaholicService.isItWeekend(dateStr), "Incorrect weekend detection for given date.");

        dateStr = "2019-11-27 10:00";
        Assert.isTrue(!workaholicService.isItWeekend(dateStr), "Incorrect weekend detection for given date.");

        dateStr = "2019-11-30 20:30";
        Assert.isTrue(workaholicService.isItWeekend(dateStr), "Incorrect weekend detection for given date.");

        dateStr = "2019-11-29 15:59";
        Assert.isTrue(!workaholicService.isItWeekend(dateStr), "Incorrect weekend detection for given date.");

        dateStr = "2019-11-29 16:00";
        Assert.isTrue(workaholicService.isItWeekend(dateStr), "Incorrect weekend detection for given date.");
    }
}
