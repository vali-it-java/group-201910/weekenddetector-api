package workaholic.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/weekenddetector")
@CrossOrigin("*")
public class WorkaholicController {

    @Autowired
    private WorkaholicService workaholicService;

    @PostMapping
    public boolean isItWeekend(@RequestParam("dateStr") String dateStr) {
        return workaholicService.isItWeekend(dateStr);
    }

    @GetMapping("/registervisit")
    public int registerVisit() {
        return workaholicService.registerVisit();
    }

}
