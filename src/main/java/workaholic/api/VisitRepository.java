package workaholic.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public class VisitRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int getVisitCount() {
        return jdbcTemplate.queryForObject("SELECT count(id) FROM visit", Integer.class);
    }

    public void addVisit() {
        jdbcTemplate.update("insert into visit (visit_time) values (?)", LocalDateTime.now());
    }
}
