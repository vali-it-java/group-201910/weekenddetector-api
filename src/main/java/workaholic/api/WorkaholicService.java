package workaholic.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class WorkaholicService {

    @Autowired
    private VisitRepository visitRepository;

//    private static int visitCount = 0;

    public boolean isItWeekend(String dateStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime date = LocalDateTime.parse(dateStr, formatter);

        if (date.getDayOfWeek() == DayOfWeek.TUESDAY ||
                date.getDayOfWeek() == DayOfWeek.WEDNESDAY ||
                date.getDayOfWeek() == DayOfWeek.THURSDAY) {
            return false;
        }  else if (date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY) {
            return true;
        } else if (date.getDayOfWeek() == DayOfWeek.MONDAY) {
            if (date.getHour() < 6) {
                return true;
            } else {
                return false;
            }
        } else {
            return date.getHour() >= 16;
        }
    }

    public int registerVisit() {
//        return ++visitCount;
        visitRepository.addVisit();
        return visitRepository.getVisitCount();
    }
}
